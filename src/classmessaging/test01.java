package classmessaging;

public class test01 {

	public test01(){
		
		ObjectPubSubV2.actions().subscribe("programstarted", this);
		ObjectPubSubV2.actions().subscribe("test", this);
		ObjectPubSubV2.actions().unsubscribe("test", this);
		
	}
	
	public void receiveMessage(String topic,Object o) throws Exception {
		
		if (topic.equals("programstarted")) {
			
			ObjectPubSubV2.actions().publish("sendHelloWorld", "Hello World");
			
		}else if (topic.equals("test")) {
			
			System.out.println("This message should not be present.");
		}
	}
	
}
