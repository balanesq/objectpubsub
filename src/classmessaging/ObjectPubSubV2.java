package classmessaging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ObjectPubSubV2 {
	
	class ExecutorAndObjectList{
		ExecutorAndObjectList(ThreadPoolExecutor executorOfSendingToObject,CopyOnWriteArrayList<Object> objectList){
			this.executorOfSendingToObject=executorOfSendingToObject;
			this.objectList=objectList;
			}
		ThreadPoolExecutor executorOfSendingToObject;
		CopyOnWriteArrayList<Object> objectList;
	}
	
	private int simultaniousThreadsPerTopic=64;
	
	public void setSimulaniousThreadsPerTopic(int in) {simultaniousThreadsPerTopic=in;};
	
	
	public void shutDownThreadPool() {
	    pubSub.forEach((k,v)->{
		v.executorOfSendingToObject.shutdown();
	    });
	}
  
    private final ConcurrentHashMap<String,ExecutorAndObjectList> pubSub=new ConcurrentHashMap<String,ExecutorAndObjectList>(); 
  
    
    /**
    * Subscribe object on specified topic 
    *
    * @ptopic  (topic could contain joker: *. Example: user/*)
    * @sendObject (object as event)
    */
    public void subscribe(final String topic,final Object sendObject) {
	
	CopyOnWriteArrayList<Object> listOfObject=null;
	
	try {
    		listOfObject=pubSub.get(topic).objectList;
	} catch (Exception e) {}
	
    	if(null==listOfObject) {
    		final CopyOnWriteArrayList<Object> newCreatedListOfObjects=new CopyOnWriteArrayList<Object>();
    		newCreatedListOfObjects.add(sendObject);
    		pubSub.put(topic, new ExecutorAndObjectList( (ThreadPoolExecutor) Executors.newFixedThreadPool(simultaniousThreadsPerTopic),newCreatedListOfObjects));
    	}else {
    		listOfObject.add(sendObject);
    	}
    }

    /**
    * Publish object on specified topic 
    *
    * @ptopic  (topic could be: send/*)
    * @object 
    */
    public void publish(final String topic,final Object sendObject){
    	
    	if ((null==topic)||(null==sendObject)) {
    		return;
    		}
    	
    	ExecutorAndObjectList eaol=pubSub.get(topic);
    	
    	if (null==eaol) {
    		return;
    		}
    	
    	CopyOnWriteArrayList<Object> listOfObject=eaol.objectList;

    	listOfObject.forEach((o)->{
            	eaol.executorOfSendingToObject.submit(() -> {
        		try {
        			Method m=o.getClass().getMethod("receiveMessage", String.class,Object.class);
        			m.invoke(o, topic,sendObject);
        			
        		} catch (NoSuchMethodException e) {
        			//throw new Exception("NoSuchMethodException");
        		} catch (SecurityException e) {
        			//throw new Exception("SecurityException");
        		} catch (IllegalAccessException e) {
        			//throw new Exception("IllegalAccessException");
        		} catch (IllegalArgumentException e) {
        			//throw new Exception("IllegalArgumentException");
        		} catch (InvocationTargetException e) {
        			//throw new Exception("InvocationTargetException");
        		}
        	});
        });
    	
    }

    
    /**
    * Unsubscribe object on specified topic 
    *
    * @ptopic  (text - string)
    * @sendObject (object as event)
    */
	public void unsubscribe(final String topic, final Object obj) {
	    ExecutorAndObjectList eaol=pubSub.get(topic);
	    CopyOnWriteArrayList<Object> listOfObject=eaol.objectList;
		listOfObject.remove(obj);
		if (listOfObject.size()==0) {
		    	eaol.executorOfSendingToObject.shutdown();
			pubSub.remove(topic);
		}
	} 
    
	/*************
	 * SingleTon *
	 ************/
	
    public static ObjectPubSubV2 actions() { 
        return SingletonHelper.INSTANCE; 
    }
	
	private static class SingletonHelper{
	        private static final ObjectPubSubV2 INSTANCE = new ObjectPubSubV2();
	}

	private ObjectPubSubV2() {}
}
