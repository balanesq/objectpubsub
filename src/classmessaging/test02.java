package classmessaging;

public class test02 {

	public test02() throws Exception{
		
		ObjectPubSubV2.actions().subscribe("sendHelloWorld", this);
		ObjectPubSubV2.actions().publish("test", "This message should not be seen becouse nobody is subscribed");
		
	}
	
	public void receiveMessage(String topic,Object o) {
		
		if (topic.equals("sendHelloWorld")) {
			
			System.out.println((String)o);
			
		}
	}
	
}
