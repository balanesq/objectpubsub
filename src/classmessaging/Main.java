package classmessaging;

public class Main {

	public static void main(String[] args) throws Exception {
		
		new test01();
		new test02();
			
		ObjectPubSubV2.actions().publish("programstarted", null);

		Thread.sleep(100);
		ObjectPubSubV2.actions().shutDownThreadPool();
	}
}
