package classmessaging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ObjectPubSub {
	
	private ThreadPoolExecutor executorOfSendingToObject = 
			  (ThreadPoolExecutor) Executors.newFixedThreadPool(64);
	
	public void shutDownThreadPool() {
	    executorOfSendingToObject.shutdown();  
	}
  
    private final ConcurrentHashMap<String,CopyOnWriteArrayList<Object>> pubSub=new ConcurrentHashMap<String,CopyOnWriteArrayList<Object>>(); 
  
    
    /**
    * Subscribe object on specified topic 
    *
    * @ptopic  (topic could contain joker: *. Example: user/*)
    * @sendObject (object as event)
    */
    public void subscribe(final String topic,final Object sendObject) {
    	
    	CopyOnWriteArrayList<Object> listOfObject=pubSub.get(topic);
    	
    	if(null==listOfObject) {
    		final CopyOnWriteArrayList<Object> newCreatedListOfObjects=new CopyOnWriteArrayList<Object>();
    		newCreatedListOfObjects.add(sendObject);
    		pubSub.put(topic, newCreatedListOfObjects);
    	}else {
    		listOfObject.add(sendObject);
    	}
    }

    /**
    * Publish object on specified topic 
    *
    * @ptopic  (topic could be: send/*)
    * @object 
    */
    public void publish(final String topic,final Object sendObject){
    	
    	pubSub.forEach(4,(topicFromList,k)->{
			
    		if (channelMech(topicFromList,topic)) {
    			
    			k.forEach((subscriberObject)->{
    				fireEvents(subscriberObject,topic,sendObject);
    			});
    		}
    	});
    }
    
    /**
    * Publish object on specified (simple) topic. This method should be faster than publish(). 
    *
    * @ptopic  (topic could not be with joker: *)
    * @object 
    */
    public void simplePublish(final String topic,final Object sendObject){
    			pubSub.get(topic).forEach((subscriberObject)->{
    				fireEvents(subscriberObject,topic,sendObject);
    			});
    }
    
    private void fireEvents(Object subscriberObject,String topic,Object sendObject) {
    	executorOfSendingToObject.submit(() -> {
			try {
				Method m=subscriberObject.getClass().getMethod("receiveMessage", String.class,Object.class);
				m.invoke(subscriberObject, topic,sendObject);
				
			} catch (NoSuchMethodException e) {
				//throw new Exception("NoSuchMethodException");
			} catch (SecurityException e) {
				//throw new Exception("SecurityException");
			} catch (IllegalAccessException e) {
				//throw new Exception("IllegalAccessException");
			} catch (IllegalArgumentException e) {
				//throw new Exception("IllegalArgumentException");
			} catch (InvocationTargetException e) {
				//throw new Exception("InvocationTargetException");
			}
		});
    }
  
    private boolean channelMech(final String channel, final String topic) {
    	
		try {
			String[] channelExploded=channel.split("/");
			String[] topicExploded=topic.split("/");
			
			if (channelExploded.length!=topicExploded.length) {
				return false;
				}
			
			for (int i=0; i < channelExploded.length; i++ ) {
				if(channelExploded[i].equals("*")||topicExploded[i].equals("*")) {
					return true;
				}else if (!channelExploded[i].equals(topicExploded[i])) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
		    return false;
		}
	}
    
    /**
    * Unsubscribe object on specified topic 
    *
    * @ptopic  (text - string)
    * @sendObject (object as event)
    */
	public void unsubscribe(final String topic, final Object obj) {
		CopyOnWriteArrayList<Object> listOfObject=pubSub.get(topic);
		listOfObject.remove(obj);
		if (listOfObject.size()==0) {
			pubSub.remove(topic);
		}
	} 
    
	/*************
	 * SingleTon *
	 ************/
	
    public static ObjectPubSub actions() { 
        return SingletonHelper.INSTANCE; 
    }
	
	private static class SingletonHelper{
	        private static final ObjectPubSub INSTANCE = new ObjectPubSub();
	}

	private ObjectPubSub() {}
}
